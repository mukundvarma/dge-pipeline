import os, sys, time

def create_uger_subfile(command, submission_dir, sample_name):
    filename = ".".join([sample_name, jobtype, time.strftime("%Y%m%d-%H%M%S"), "sh"])
    os.system("cp ~varma/DGE-Pipeline/uger_template.sh %s/%s" % (submission_dir,filename))
    filepath = os.path.join(submision_dir, filename)
    subfile = open(filepath, "a")
    subfile.write(command)
    subfile.close()
    os.system("sed -i 's/JOBNAME/%s/g' %s" % (jobtype+"_"+sample_name, filepath))
