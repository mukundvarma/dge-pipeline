import os, sys, subprocess

def list_files(read_number):
    read_list = subprocess.check_output(["ls *_R%s_*"])%(read_number)
    return read_list
    
def remove_undetermined(read_list):
    for item in read_list:
        if (item.find("Undetermined") != -1):
            read_list.remove(item)
    return read_list

def main():
    readList = []
    readsText = []
    for i in range(2):
        readList.extend(list_files(i+1))
        remove_undetermined(readList[i])
        readsText = ",".join(readList[i])
    output = " ".join(readsText)
    return output
    

