c = read.delim("Counting/counts.total.dat", header=T, row.names=1)
newc = data.frame(matrix(nrow = nrow(c), ncol = ncol(c)/4))
rownames(newc) = rownames(c)
type = unlist(lapply(colnames(c), strsplit, "_"))[3*(1:ncol(newc)) - 1]
number = unlist(lapply(colnames(c), strsplit, "_"))[3*(1:ncol(newc))]
colnames(newc) = paste(type, number, sep="_")
colnames(newc)
for (i in 1:30) {newc[,i] = c[,i] + c[,30+i] + c[,60+i]+ c[,90+i]}
write.table(newc, "counts.table.merged.dat", quote=F, sep = "\t")

