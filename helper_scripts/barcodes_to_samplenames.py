import os

def rename_files(file_list, reads_dir, barcodes):
    extn = ".fastq.gz"
    for filename in file_list:
    #readnum is the read number _R1 (left read) or _R2 (right read)
        if (filename.endswith("_R1.fastq") or filename.endswith("_R2.fastq")):
            continue
        readnum = filename[len(filename)-10: len(filename)-9]
        print filename, readnum

        filenum = filename[:2]
        for sample in barcodes:
            if barcodes[sample] in filename:
                new_filename = ''.join([filenum, sample + "_R" + str(readnum) + extn])
                movecommand = "mv %s %s" % (os.path.join(reads_dir, filename), os.path.join(reads_dir, new_filename))
                print movecommand
                if new_filename not in fn_array:
                    os.system(movecommand)
                    break
                else:
                    print "Error file already exists, fuck you Mukund"
    
def create_barcode_dict(barcodes_file):
    barcodes = {}
    for line in barcodes_file:
        line.strip()
        # Split the line along whitespace
        # Note: this fails if your filenames have whitespace
        sample_name, index_1, index_2 = line.split('\t')
        combined_index = index_1+"_"+index_2
        if sample_name not in n_samples:
            n_samples[sample_name] = 1
        else:
            n_samples[sample_name] += 1
        sample_full = sample_name+"_S"+str(n_samples[sample_name])
        barcodes[sample_full] = combined_index
    print barcodes
    return barcodes


# A dict with sample names and barcodes, and another in case there are many barcodes associated with the same sample
barcodes_B = {}
#barcodes_L = {}
n_samples = {}
fn_array = {}
extn = ".fastq.gz"
reads_dir = "Reads/"   #Hard-coded for now
barcodes_file_B = open('barcodes.txt').read().splitlines()
#barcodes_file_L = open('barcodes2.txt').read().splitlines()
#barcodes_L = create_barcode_dict(barcodes_file_L)
barcodes_B = create_barcode_dict(barcodes_file_B)
filelist_all = os.listdir(reads_dir)
#filelist_L = []
#filelist_B = []
#for readfile in filelist_all:
   # if readfile.startswith('1_'):
  #      filelist_B.append(readfile)
#    else:
 #       filelist_L.append(readfile)

#rename_files(filelist_L, reads_dir, barcodes_L)
rename_files(filelist_all, reads_dir, barcodes_B)

# Read through the mapping file line-by-line and populate the barcode dictionary
#print barcodes
# List the files in the current directory
