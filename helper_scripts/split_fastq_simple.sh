for file in *.fastq
do
    split -l 24000000 $file ${file%.*}
    mv $file done/
done
for splits in $(find . -type f ! -name "*.*")
do
    mv "$splits" "$splits.fastq"
done
