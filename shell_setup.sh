#!
#Create working directory
mkdir "Alignment"
mkdir "Counting"
mkdir "QC"
mkdir "EdgeR"
mkdir "GSEA"
mkdir "Report"
mkdir "OutputLogs"
mkdir "ErrorLogs"
mkdir "Submission"

#Create QC subdirectories
mkdir QC/BamStats
mkdir QC/ReadsDistribution

#Create published directories

mkdir 
