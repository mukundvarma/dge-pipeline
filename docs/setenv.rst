Setting up your environment 
***************************

Before you can run the pipeline, you need to 

#. Download the code for the pipeline  	
#. Add the location of the code to your PATH variable
#. Load the "Dotkits" or softwares that are installed on Broad servers

Obtaining the code for the pipeline
===================================
Bitbucket repository: https://bitbucket.org/mukundvarma/dge-pipeline/
Git tutorial: https://try.github.io/

The rest of this section assumes that the code is in a directory called DGE-Pipeline in your home directory on Broad, e.g. 
:file:`/home/unix/varma/DGE-Pipeline` .


Modify your PATH variable
=========================
You can add the path of the directory where you downloaded the code to your PATH variable in the unix shell. This way, if you want to run a script, say, script.py without specifying its complete path.

.. code-block:: bash
  
   export PATH=$PATH:~/DGE-Pipeline/
   export PATH=$PATH:/home/unix/varma/DGE-Pipeline/helper_scripts

   #version of samtools and bowtie installed on boltzmann is outdated, so you might need to use the following versions, place this in your ~/.my.bashrc file
   PATH=$PATH:/broad/xavierlab_datadeposit/varma/bin/samtools-0.1.19
   PATH=$PATH:/broad/xavierlab_datadeposit/varma/bin/RSEM-1.2.25
   PATH=$PATH:/broad/xavierlab_datadeposit/varma/bin/bowtie-1.1.2
   PATH=$PATH:/broad/xavierlab_datadeposit/varma/bin/fastx/bin
   PATH=$PATH:/broad/xavierlab_datadeposit/varma/bin/broad/software/free/Linux/redhat_5_x86_64/pkgs/python_2.7.1-sqlite3-rtrees/bin
   
   #some python packages might not be installed on boltzmann
   PYTHONPATH=$PYTHONPATH:/home/unix/varma/.local/lib/python2.7/site-packages/


Loading required Dotkits
========================

.. code-block:: bash

   reuse -q LSF
   reuse -q Python-2.7
   reuse -q R-3.1 
   reuse -q BWA
   reuse -q Tophat
   reuse -q Bowtie
   reuse -q GCC-4.9
   reuse -q Emacs
   reuse -q Git-1.7

