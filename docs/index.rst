.. XRnaSeqPipeline documentation master file, created by
   sphinx-quickstart on Mon Jul 20 14:44:09 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Xavier Lab pipeline for RNASeq analysis
===========================================

Contents:

.. toctree::
   :maxdepth: 2

   intro.rst
   dirstructure.rst
   setenv.rst
   runpipeline.rst
   analysis.rst
   underthehood.rst
   faq.rst
   help.rst	     


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

