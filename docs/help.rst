Getting help
************

If you can't find the answers to your questions in this document, 
you can send an email to varma@broadinstitute.org, with the 
subject "RNASeq pipeline help" (without the quotes) 
