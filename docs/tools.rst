Tools and softwares used in the pipeline
****************************************

The following free and open-source tools and softwares were used in the pipeline.
We thank the developers for making their work freely available to the 
computational biology community

* Alignment: TopHat, Bowtie, BWA
* Counting: htseq
* Differential Gene Expression analysis: edgeR
* QC Analysis: RSeqC, FastQC
* Graphics: ggplot2, pheatmap
* Documentation: Sphinx, Nozzle


