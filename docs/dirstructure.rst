Setting up your working directory
*********************************

The pipeline looks for input files in very particular locations, so it is very important
that the instructions for setting up your working directory are followed meticulously.

Reads Directory
===============

The term working directory will refer to the top-level directory in which all other subdirectories 
and files needed for this analysis need to be placed. 
Usually, the starting point for your analysis will be a set of fastq files created by
a sequencing platform. In this case, all the fastq files need to be placed in a 
subdirectory named *Reads*. If instead your primary input is a set of aligned bam files,
you need to place all the bam files in a subdirectory named *Alignment*.

Text files
==========
In addition, the following text files describing your data need to be created and placed in 
your working directory. Please take note that regardless of how your experiment is set up, the presence
of these files is necessary for the proper functioning of the pipeline at the current time. In some cases,
the steps requiring one or more of these files might not be needed. However, 90% of the time this will not
be the case and you **must** still create an empty file by this name if you do not have the information that
these files should contain.


read_pairs.txt
--------------
Each line of this file should contain the names of the two files corresponding to the 
two ends of a paired end sequencing experiment. Usually, paired reads will have the same prefix followed 
by R1 (for the left reads) and R2 (for the right reads). Each line of read_pairs.txt should contain only 
the names of these two files separated by a tab-character.The code block below shows what a read_pairs.txt
could look like.

.. code-block:: python
   :emphasize-lines: 1,2,3,4,5
   :caption: :file:`read_pairs.txt`

   B6L4_S1_R1.fastq	B6L4_S1_R2.fastq
   B6L4_S2_R1.fastq	B6L4_S2_R2.fastq
   B6L4_S3_R1.fastq	B6L4_S3_R2.fastq
   B6L8_S1_R1.fastq	B6L8_S1_R2.fastq


samples_described.txt
---------------------
The dataset you have might contain multiple fastq files all corresponding to the same
sample-type. This makes it necessary to describe all the fastq files, so that the gene counts for each of these
different files can be combined when constructing the counts matrix. The samples_described.txt file does this job.
Each line of this file contains the sample-type and the filename prefix (as described in the previous bullet-point)
separated by a tab-character. The samples_described.txt file for our example experiment is shown below.

.. code-block:: python
   :emphasize-lines: 1,2,3,4,5
   :caption: :file:`samples_described.txt`

   B6S4    B6S4_S1
   B6S4    B6S4_S2
   B6S4    B6S4_S3
   B6S8    B6S8_S1

   
barcodes.txt
------------
Samples in RNAseq experiments are identified by their barcodes. If the fastq files are not demultiplexed by sample,
then the barcode information is required. This file will contain the sample-type, the i7 index barcode and the i5 index
barcode (in that order) separated by tab-characters. If you do not have an i5 index (e.g. for 3'DGE), then you just need
the first two columns. A barcodes.txt file is shown below.

.. code-block:: python
   :emphasize-lines: 1,2,3,4,5
   :caption: :file:`barcodes.txt`

   B6S4    TAAGGCGA    CTCTCTAT
   B6S4    CGTACTAG    CTCTCTAT
   B6S4    AGGCAGAA    CTCTCTAT
   B6S8    TCCTGAGC    CTCTCTAT
	     
samples_compared.txt
--------------------
This file is required only if you are performing pairwise differential gene expression. However, the program still looks
to see if it exists before running, so it is recommended that you create an empty file by this name even if you don't wish to
perform the DGE analysis. If you are performing this kind of an analysis, each line of this file should contain one comparison
expression in the format sample1_vs_sample2. For example,

.. code-block:: python
   :emphasize-lines: 1
   :caption: :file:`samples_compared.txt`

   B6S4_vs_B6S8
