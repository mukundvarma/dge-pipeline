Using the pipeline
********************

Conducting a test run
=====================
Before you run the pipeline on your entire dataset, it is recommended that you do so on a small subset of it. 
This helps identify incompatibilities between the pipeline and the data at an early stage, saving you precious 
time and computing resources. This is particularly true if this is your first time running the pipeline. 

The pipeline contains a script in the :file:`helper_scripts/` directory which creates smaller files from your 
fastq files and runs the first three steps of the pipeline on them (input validation, alignment, counting). 

.. code-block:: bash
   :caption: :file:`test_run.sh`

   #! /bin/bash
   rm -r Test #remove results of any previous test runs
   mkdir Test #make test directory

   #set up working directory
   mkdir Test/Reads
   cp read_pairs.txt Test/
   cp barcodes.txt Test/
   cp samples_described.txt Test/ 

   #create smaller files to test pipeline on
   for file in $(cat Test/read_pairs.txt); do
       if [ ${file: -2} == "gz" ]
       then
           cat Reads/$file | gunzip | head -20000 | gzip -c > Test/Reads/$file
       else
           cat Reads/$file | head -20000 > Test/Reads/$file
       fi
   done;

   script_dir=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
   cd Test
   
   #Run test
   python $script_dir/composite_pipeline.py --nolsf --organism $1 --start 1 --end 3
   
If the count matrix and alignment files are of non-zero size, then you are good to run the pipeline on your entire dataset!

Running a full analysis
=======================

The pipeline is run by a single script :file:`composite_pipeline.py` which calls on various other scripts and 
programs for the various steps. The steps are: 

Steps
-----

#. Validate Input
#. Align fastq files to reference genome
#. Create a count matrix
#. QC analysis on bam files and/or count matrix
#. Differential expression analysis using edgeR
#. Generate html report 

These steps are described in detail in the section :doc:`underthehood` . For now, we 
just explain how you can run your analysis.

Usage
-----
.. code-block:: bash

   #if you added the path to where you downloaded the code for your pipeline to your PATH (see "Setting up your environment")
   python composite_pipeline.py [options]
   #otherwise
   python /path/to/script/dir/composite_pipeline.py [options]

The pipeline takes various arguments, some required, some not. These are described below:

Options
^^^^^^^

.. code-block:: bash

   --organism		The name of the organism. Supported arguments are **mouse** (default) and human

   --dge		(required) 3'DGE or Whole Transcriptome. The pipeline uses different softwares for analysis of 3'DGE 
			and Whole Transcriptome data (most notably, the aligners). Supported arguments are **1** (Whole 
			Transcriptome) and **3** (3'DGE)
 
   --se			Single-end or Paired-end. This option is valid only for whole transcriptome data. Supported arguments 
			are **0** (paired-end, default) or **1** (single-end)

   --start		Index of the starting step. The default is **1**

   --end		Index of the ending step. The default is **4**

   --nolsf		Do not use LSF cluster. By default, the pipeline submits jobs to the LSF cluster to run in 
			parallel on each of your fastq files. However, it is also possible to run it locally on the 
			machine you are submitting the jobs from (usually boltzmann.broadinstitute.org). You may want 
			to do this for debugging purposes. Use this argument if you wish to run your analysis locally

   --fa			Run functional analysis. If you're doing a DE study, it is possible to run functional analysis 
			as well. However, this is a very time consuming step, so you might not want to run this unless you 
			really need to do functional analysis. Use this option if you wish to run functional analysis 
			
   
Example
^^^^^^^

.. code-block:: bash

   python composite_pipeline.py --organism mouse --dge 3 --start 1 --end 5 --nolsf --fa
   #the defaults (as described in the previous section) are assumed for unspecified options

Monitoring the pipeline
=======================
There are various ways to check pipeline progress:

1. Using "top": 

   * Alignment will show up as a bwa or tophat process.

   * Counting will show up as a python process.

   * EdgeR will show up as an R process. 

2. Using "bjobs": 

   * Splitting will spawn a bunch of nodes running python.

   * Alignment will spawn a bunch of nodes running bwa or tophat.

   *  GSEA will spawn a bunch of nodes running java.

3. Checking what files have been created in your project directory.

   If the pipeline dies prematurely, logging output will appear in nohup.out (or whatever
   log file you piped the command to). That should give you a sense of what went wrong.
   Note that the default log will append to (rather than overwrite) nohup.out, so make 
   sure you're looking at the right stack trace!
