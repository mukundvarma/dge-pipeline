#! /bin/sh

#$ -cwd
#$ -q QUEUE
#$ -N JOBNAME.JOBTYPE
#$ -l m_mem_free=32g
#$ -e ErrorLogs/JOBNAME.err
#$ -o OutputLogs/JOBNAME.out

source /broad/software/scripts/useuse 
reuse -q Python-2.7
reuse -q R-3.2
reuse -q GCC-4.8
reuse -q Tophat
reuse -q BWA
PATH=$PATH:/broad/xavierlab_datadeposit/varma/bin/bowtie-1.1.2:/broad/xavierlab_datadeposit/varma/bin/samtools-0.1.19:/broad/xavierlab_datadeposit/varma/bin/broad/software/free/Linux/redhat_5_x86_64/pkgs/python_2.7.1-sqlite3-rtrees/bin:/home/unix/varma/DGE-Pipeline/RSeqC/bin
PYTHONPATH=$PYTHONPATH:/home/unix/varma/.local/lib/python2.7/site-packages/:/broad/xavierlab_datadeposit/varma/bin/broad/software/free/Linux/redhat_5_x86_64/pkgs/python_2.7.1-sqlite3-rtrees/lib/python2.7/site-packages:/home/unix/varma/DGE-Pipeline/RSeqC/lib/python2.7/site-packages
