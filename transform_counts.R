script_dir = dirname(regmatches(commandArgs(), regexpr("(?<=^--file=).+", commandArgs(), perl=TRUE)))
library(preprocessCore, lib.loc=file.path(script_dir,'/R/lib'))
library(limma, lib.loc=file.path(script_dir,'/R/lib'))
library(edgeR, lib.loc=file.path(script_dir,'/R/lib'))
library(DESeq2, lib.loc=file.path(script_dir,'/R/lib'))

write_quantiles <- function(input.file, output.file) {
	counts = read.delim(input.file, header=TRUE, row.names=1)
	rows = rownames(counts)
	cols = colnames(counts)
	quantiles = normalize.quantiles(data.matrix(counts))
	rownames(quantiles) <- rows
	colnames(quantiles) <- cols
	write.table(quantiles, file=output.file, sep='\t', quote=FALSE)
}

write_cpm <- function(input.file, output.file) {
	counts = read.delim(input.file, header=TRUE, row.names=1)
	cpms = cpm(counts)
	write.table(cpms, file=output.file, sep='\t', quote=FALSE)
}

write_rlog <- function(samples.file, counts.file, blind.file, unblind.file) {
	counts = read.delim(counts.file, header=TRUE, row.names=1)
	samples = read.delim(samples.file, row.names=2, header=FALSE)
	samples = data.frame(row.names = colnames(counts), Condition = samples[colnames(counts),])
  	deseq = DESeqDataSetFromMatrix(counts, samples, ~Condition)
    deseq = DESeq(deseq)
    rlog.blind = assay(rlog(deseq, blind=TRUE))
    rlog.unblind = assay(rlog(deseq, blind=FALSE))
    write.table(rlog.blind, file=blind.file, sep='\t', quote=FALSE)
    write.table(rlog.unblind, file=unblind.file, sep='\t', quote=FALSE)
}

# main
args = commandArgs(TRUE)
base_dir = args[1]

setwd(file.path(base_dir, "Counting"))

samples = "../samples_described.txt"
counts.umi = "counts.umi.dat"
counts.total = "counts.total.dat"
quantiles.umi = "quantiles.umi.dat"
quantiles.total = "quantiles.total.dat"
cpm.umi = "cpm.umi.dat"
cpm.total = "cpm.total.dat"
rlog.blind.umi = "rlog_blind.umi.dat"
rlog.blind.total = "rlog_blind.total.dat"
rlog.unblind.umi = "rlog_unblind.umi.dat"
rlog.unblind.total = "rlog_unblind.total.dat"

write_quantiles(counts.umi, quantiles.umi)
write_quantiles(counts.total, quantiles.total)
write_cpm(counts.umi, cpm.umi)
write_cpm(counts.total, cpm.total)
write_rlog(samples, counts.umi, rlog.blind.umi, rlog.unblind.umi)
write_rlog(samples, counts.total, rlog.blind.total, rlog.unblind.total)