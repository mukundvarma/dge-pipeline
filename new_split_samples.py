from __future__ import print_function
import os, sys, gzip, re, itertools, argparse

class MultiFastqReader:
	"""
	Reads FASTQ files in parallel.
	"""
	def __init__(self, *fastq_paths):
		self.fastqs = list()
		for f in fastq_paths:
			self.fastqs.append(self.open_fastq_or_gz(f))

	def __enter__(self):
		return self

	def __exit__(self, type, value, traceback):
		for f in self.fastqs:
			f.close()

	def next(self):
		zipped = itertools.izip(*self.fastqs)
		for headers in zipped:
			seqs = zipped.next()
			pluses = zipped.next()
			quals = zipped.next() #Each read consists of 4 lines - header, sequence, "+", quality information about the read
			#This method separates the read into components and strips them of stray spaces

			clean = lambda x: x.strip()
			headers = map(clean, headers)
			seqs = map(clean, seqs)
			quals = map(clean, quals)

			yield (headers, seqs, quals)

	@staticmethod
	def open_fastq_or_gz(filename):
		if filename.endswith(".fastq") and os.access(filename, os.F_OK):
			return open(filename, "rU")
		elif filename.endswith(".fastq.gz") and os.access(filename, os.F_OK):
			return gzip.open(filename, "rb")
		elif filename.endswith(".fastq") and os.access(filename + ".gz", os.F_OK):
			return gzip.open(filename + ".gz", "rb")
		elif filename.endswith(".fastq.gz") and os.access(filename[:-3], os.F_OK):
			return open(filename[:-3], "rU")
		raise IOError, "Unknown file: " + filename

class BufferedFastqWriter:
	def __init__(self, base_name, out_dir, block_size=10000000):
		self.block_size = block_size
		self.total_size = 0
		self.out_dir = out_dir
		self.base_name = base_name
		self.buffer = list()

	def __enter__(self):
		return self

	def __exit__(self, type, value, traceback):
		if len(self.buffer) > 0:
			self.write_fastq()

	def push(self, name, seq, qual):
		self.buffer.append((name, seq, qual))
		self.total_size += 1
		if self.total_size % self.block_size == 0:
			self.write_fastq()
			self.buffer = list()

	def write_fastq(self):
		fastq_path = os.path.join(self.out_dir, "%s.%s.fastq" % ( self.total_size, self.base_name))
		with open(fastq_path, "w") as out:
			for name, seq, qual in self.buffer:
				print("\n".join([name, seq, "+", qual]), file=out)

def get_tag_info(read_seq, read_qual, dge = 3, do_mask=True):
	#input is R1 if 3' and R2 if 5'
	if dge == 3:
		if do_mask:
			tag = mask(read_seq[0:6], read_qual[0:6], min_qual=10)
			umi = mask(read_seq[6:16], read_qual[6:16], min_qual=30)
		else:
			tag = read_seq[0:6]
			umi = read_seq[6:16]
	else:
		if do_mask:
			tag = mask(read_seq[6:12], read_qual[6:12], min_qual = 10)
			umi = mask(read_seq[12:19], read_seq[12:19], min_qual=10)
		else:
			tag = read_seq[6:12]
			umi = read_seq[12:19]
	return tag, umi

def mask(seq, qual, min_qual=30):
	# Mask sequence by quality score
	return "".join((b if (ord(q) - 33) >= min_qual else "N") for b, q in itertools.izip(seq, qual))

def get_plate_mapping(barcode_file):
	# figure out the plate -> index mapping
	# (in case we're using MiSeq data, with no indices listed in FASTQs)
	mapping = dict()
	with open(barcode_file, "r") as f:
		for line in f:
			fields = line.strip()
			if len(line) > 0:
				plate_well, tag, index = line.split()
				if plate_well.startswith("N7"):
					plate_num = str(int(plate_well[2:4]))
					mapping[plate_num] = index
	return mapping

def run(reads_dir, alignment_dir, barcode_file, r1_fastq, r2_fastq, idx_fastq, dge):
#if dge = 3, protocol = 0, if dge = 5, protocol = 1
	if (dge == 3):
		protocol = 0
	else:
		protocol = 1
	plate_map = get_plate_mapping(barcode_file)
	numeric = re.compile(r"^[0-9]+$")
	sequence = re.compile(r"^[acgtnACGTN]+$")
	dummy_index = "NNNNNNNN"

	if idx_fastq is not None:
		input_files = [r1_fastq, r2_fastq, idx_fastq]
		has_sep_idx = True
	else:
		input_files = [r1_fastq, r2_fastq]
		has_sep_idx = False

	output_base = os.path.basename(r2_fastq).split('.gz')[0].split('.fastq')[0]
	input_paths = [os.path.join(reads_dir, fastq) for fastq in input_files]

	with BufferedFastqWriter(output_base, alignment_dir) as writer:
		with MultiFastqReader(*input_paths) as reader:
			for (headers, seqs, quals) in reader.next():
				tag, umi = get_tag_info(seqs[protocol], quals[protocol], dge) # get tag/UMI from read 1 if 3' and read 2 if 5'
				index = (headers[1].split()[1]).split(':')[3] # get index from read 2
				head = headers[1].split()[0] # get header from read 2

				if has_sep_idx:
					index = seqs[2] # get from index read
				elif re.search(numeric, index) is not None:
					index = plate_map.get(index, dummy_index)
				elif re.search(sequence, index) is None:
					index = dummy_index

				new_name = ":".join([head, index, tag, umi])
				new_seq, new_qual = seqs[1 - protocol], quals[1 - protocol] # get sequence from read 2 if 3' and read 1 if 5'
				writer.push(new_name, new_seq, new_qual)

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("dge", type=int)
	parser.add_argument("reads_dir")
	parser.add_argument("alignment_dir")
	parser.add_argument("barcode_file")
	parser.add_argument("r1_fastq")
	parser.add_argument("r2_fastq")
	parser.add_argument("--idx_fastq")
	args = parser.parse_args()

	run(args.reads_dir, args.alignment_dir, args.barcode_file,
	    args.r1_fastq, args.r2_fastq, args.idx_fastq, args.dge)

if __name__ == '__main__':
	main()
