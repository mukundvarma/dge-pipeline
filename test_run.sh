#! /bin/bash
rm -rf Test #remove results of any previous test runs
mkdir Test #make test directory

#set up working directory
mkdir Test/Reads

cp read_pairs.txt Test/
cp barcodes.txt Test/
cp samples_described.txt Test/

#create smaller files to test pipeline on
for file in $(cat Test/read_pairs.txt); do
    if [ ${file: -2} == "gz" ]
    then
        cat Reads/$file | gunzip | head -10000 | gzip -c > Test/Reads/$file
    else
        cat Reads/$file | head -10000 > Test/Reads/$file
    fi
done;

script_dir=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
cd Test

#Run test
echo "Running UGER Pipeline locally for 2500 reads assuming paired end whole transcriptome data for mice cells" 
python $script_dir/uger_pipeline.py --nogrid --organism mouse --start 1 --end 4 --dge 1
