#! /bin/bash

#$ -cwd
#$ -q long
#$ -N demultiplex
#$ -l h_vmem=32g

set -x

source /broad/software/scripts/useuse

# Change datadir to the top-level directory containing raw bcl files
# and outdir to nextseq/Reads
datadir=/path/to/data/dir
outdir=/path/to/output/dir
mkdir -p ${outdir}

echo ${datadir}

use .bcl2fastq2-2.17.1.14
nohup bcl2fastq --runfolder-dir ${datadir} --output-dir ${outdir} --mask-short-adapter-reads 10 --minimum-trimmed-read-length 10 --no-lane-splitting



