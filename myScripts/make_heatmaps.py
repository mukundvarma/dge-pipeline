import os, sys, re, shutil, argparse

stimnames = open("stimnames.txt")
pmode = 0
#print(pmode)
for stimname in stimnames:
    os.system("Rscript ~/DGE-Pipeline/dge-pipeline/megaTable.R %s %s" % (pmode, stimname))
#    print(pmode, stimname)
os.system("mkdir -p Plots")
os.system("mv *.png Plots/")
os.system("mv *.ifntable.tsv Ifntables/")
