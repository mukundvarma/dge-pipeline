#!/usr/bin/env python

import os, sys, re, shutil, argparse, time, subprocess

#environment info
host = os.uname()[1]
ostype = os.uname()[0]
pwd = subprocess.check_output("pwd").strip()
exp_name = "_".join(pwd.split("/")[-2:]).strip()
	
# General methods
# ===========================================================================================

def path_from_script_dir(subpath):
	# originally /broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts/<subpath>
	return os.path.join(os.path.abspath(os.path.dirname(__file__)), subpath)

def soft_mkdir(mydir):
	if not os.path.isdir(mydir):
		os.mkdir(mydir)

def create_uger_subfile(command, submission_dir, sample_name, jobtype, queue):
	global exp_name
	filename = ".".join([sample_name, jobtype, "sh"])
	filepath = os.path.join(submission_dir, filename)
	if not os.path.isfile(filepath):
		os.system("cp %s %s/%s"  % (path_from_script_dir("uger_template.sh"), submission_dir,filename))
	subfile = open(filepath, "a")
	subfile.write(command+"\n")
	subfile.close()
	jobname = "_".join([jobtype[:5], exp_name[5:12], sample_name])
	print jobname
	os.system("sed -i -e 's/JOBTYPE/%s/g' -e 's/JOBNAME/%s/g' -e 's/QUEUE/%s/g' %s" % (jobtype, jobname, queue, filepath))
	return filepath		

def grid_submit(qsub_cmd):
	global host
	global pwd
	if (host == "boltzmann"):
		print host, pwd
		os.system("ssh gold.broadinstitute.org 'cd %s; %s'"%(pwd, qsub_cmd))
	else:
		os.system(qsub_cmd)

def files_for_organism(organism):

	base_dir = path_from_script_dir("/broad/xavierlab_datadeposit/GenomeReference/")

	if organism.lower() == 'mouse':
		refseq_file = os.path.join(base_dir, "Mouse_RefSeq/refMrna_ERCC_polyAstrip.mm10.fa")
		sym2ref_file = os.path.join(base_dir, "Mouse_RefSeq/refGene.mm10.sym2ref.dat")
		hom_file = os.path.join(base_dir, "mouse_to_human")
		gtf_file = "/seq/references/Mus_musculus_assembly10/v0/annotation/Mus_musculus_assembly10.gtf"
		reference_prefix = "/seq/references/Mus_musculus_assembly10/v0/tophat/Mus_musculus_assembly10"
		transcriptome_prefix = "/broad/xavierlab_datadeposit/varma/reference/Mus_musculus_assembly10"
		reference_fasta = "/seq/references/Mus_musculus_assembly10/v0/Mus_musculus_assembly10.fasta"	
		bed_file = "/broad/xavierlab_datadeposit/varma/reference/mm10_RefSeq.bed"
	
	elif organism.lower() == 'human':
		refseq_file = os.path.join(base_dir, "Human_RefSeq/refMrna_ERCC_polyAstrip.hg19.fa")
		sym2ref_file = os.path.join(base_dir, "Human_RefSeq/refGene.hg19.sym2ref.dat")
		hom_file = None
		gtf_file = "/seq/references/Homo_sapiens_assembly19/v1/annotation/Homo_sapiens_assembly19.gtf"
		reference_prefix = "/seq/references/Homo_sapiens_assembly19/v1/tophat/Homo_sapiens_assembly19"
		transcriptome_prefix = "/broad/xavierlab_datadeposit/varma/reference/Homo_sapiens_assembly19"
#		transcriptome_prefix = "/seq/references/Homo_sapiens_assembly19/v1/annotation"
		reference_fasta = "/seq/references/Homo_sapiens_assembly19/v1/Homo_sapiens_assembly19.fasta"
		bed_file = "/broad/xavierlab_datadeposit/varma/reference/hg19_RefSeq_nochr.bed"
	else:
		raise Exception("Unknown species")

	return refseq_file, sym2ref_file, hom_file, reference_prefix, gtf_file, transcriptome_prefix, reference_fasta, bed_file

def get_read_names(read_pairs_file):
	read_names = list()
	with open(read_pairs_file, 'r') as reads:
		for line in reads:
			fields = line.split("\t")
			r1_fastq = fields[0]
			r2_fastq = None
			idx_fastq = None
			if len(fields) >= 2:
				r2_fastq =fields[1]
			if len(fields) == 3:
				idx_fastq =fields[2]
			read_names.append((r1_fastq, r2_fastq, idx_fastq))
	return read_names

def notify(text):
	output = """#######################################################################
######## %s
#######################################################################"""%text
	
	print output

def wait_for_previous_step():
	sp_out = subprocess.check_output(['qstat', '-xml'])
	while sp_out.find(exp_name[5:12]) != -1:
		time.sleep(60)
		sp_out = subprocess.check_output(['qstat', '-xml'])
	return

# Formatting output
# -------------------------------------------------------------------------------------------

def sed_format(outfile):
	sed_cmd = "sed -i -e 's/ \+ /\t/g' -e 's/ /_/g' -e 's/^=/#=/g' %s" % (outfile)
       	return(sed_cmd)


# Command generation
# ==========================================================================================

def make_validate_cmd(read_pairs_file, barcodes_file, 
	samples_described_file, samples_compared_file, reads_dir):
	python = "python " + path_from_script_dir("validate_inputs.py")
	cmd = " ".join([python, read_pairs_file, barcodes_file, 
		samples_described_file, samples_compared_file, reads_dir])
	return cmd

def make_split_cmd(reads_dir, alignment_dir, barcode_file, r1_fastq, r2_fastq, idx_fastq, dge):
	python = "python " + path_from_script_dir("new_split_samples.py")
	cmd = " ".join([python, str(dge), reads_dir, alignment_dir, barcode_file, r1_fastq, r2_fastq])
	if idx_fastq is not None:
		cmd += " --idx_fastq "+idx_fastq
	return cmd

def make_bwa_cmd(fastq_path, reference_prefix, alignment_dir):
	# bwa = "/broad/software/free/Linux/redhat_5_x86_64/pkgs/bwa_0.7.4/bwa"
	bwa = "bwa"
	aln = "aln -l 24 %s %s" % (reference_prefix, fastq_path)
	samse = "samse %s - %s > %s.sam" % (reference_prefix, fastq_path, fastq_path)
	cmd = " ".join([bwa, aln, "|", bwa, samse])
	return cmd

def make_tophat_cmd(left_fastq, right_fastq, gtf_file, transcriptome_prefix, reference_prefix, 
		    alignment_dir, se):
	if (se == 1):
		lt = "fr-firststrand"
	elif(se == 2):
		lt = "fr-secondstrand"
	else:
		lt = "fr-unstranded"
		
        tophat = "tophat"
        opts = "--no-sort-bam --bowtie1 --no-coverage-search --no-novel-juncs"#remove bowtie1
	rgid = "--rg-id %s --rg-sample %s" % (left_fastq[:8], left_fastq[:8])
	gtf = "-G %s" % (gtf_file)
	transcriptome = "--transcriptome-index %s" %(transcriptome_prefix)
	librarytype = "--library-type %s" % (lt)
        out = "-o %s"%(alignment_dir)
        cmd = " ".join([tophat, opts, librarytype, rgid, out, gtf, transcriptome, reference_prefix, left_fastq, right_fastq])
        return cmd

def make_count_cmd(sym2ref_file, barcode_file, alignment_dir, counting_dir, dge):
	python = "python " + path_from_script_dir("new_merge_and_count.py")
	cmd = " ".join([python, sym2ref_file, barcode_file, alignment_dir, counting_dir, str(dge)])
	return cmd

def make_rnk_cmd(edger_dir, rnk_dir, hom_file):
	python = "python " + path_from_script_dir("edger_to_rnk.py")
	cmd = " ".join([python, edger_dir, rnk_dir])
	if hom_file is not None:
		cmd += " --hom_file "+hom_file
	return cmd

## GSEA currently unused. Analysis is done in the "Report" step by nozzle.R
def make_gsea_cmd(rank_file, gmt_file, out_dir):
	java = "java -cp"
	jar = path_from_script_dir("java/gsea2-2.1.0.jar")
	memreq = "-Xmx1G"
	module = "xtools.gsea.GseaPreranked"
	global_pars = " ".join(["-collapse false",
							"-set_min 15",
							"-set_max 500",
							"-scoring_scheme weighted",
							"-nperm 1000",
							"-norm meandiv",
							"-mode Max_probe",
							"-include_only_symbols true",
							"-rnd_seed timestamp"
						])
	input_pars = "-gmx %s -rnk %s" % (gmt_file, rank_file)
	output_file = "_".join([os.path.join(out_dir, os.path.basename(rank_file)).split(".rnk")[0], 
		os.path.basename(gmt_file), "output"])
	output_pars = "-out %s" % output_file 
	#log_pars = "> %s" % os.path.join(out_dir, "gsea.log")
	cmd = " ".join([java, jar, memreq, module, global_pars, input_pars, output_pars])
	return cmd

# Pipeline steps
# ==========================================================================================


def run_validate(read_pairs_file, barcodes_file, 
		samples_described_file, samples_compared_file, reads_dir):
	os.system(make_validate_cmd(read_pairs_file, barcodes_file, samples_described_file, 
				    samples_compared_file, reads_dir))


def run_split(read_pairs_file, barcodes_file, reads_dir, alignment_dir, submission_dir, nogrid, dge, queue):
	for (r1_fastq, r2_fastq, idx_fastq) in get_read_names(read_pairs_file):
		print r1_fastq, r2_fastq, idx_fastq
		sample_name = r1_fastq[:20]
		split_cmd = make_split_cmd(reads_dir, alignment_dir, barcodes_file, r1_fastq, 
					   r2_fastq, idx_fastq, dge)
		if nogrid:
			os.system(split_cmd)
		else:
			subfile = create_uger_subfile(split_cmd, submission_dir, sample_name, 
						      "Split", queue)
			grid_submit("qsub %s"%subfile)
	if not nogrid: 
		wait_for_previous_step()

# Alignment modules 
# ------------------------------------------------------------------------------------------
		
def run_bwa(reference_prefix, alignment_dir, submission_dir, nogrid, queue):

	for fastq in filter(lambda x: x.endswith('.fastq'), os.listdir(alignment_dir)):
		fastq_path = os.path.join(alignment_dir,fastq)
		bwa_cmd = make_bwa_cmd(fastq_path, reference_prefix, alignment_dir)
		sample_name=fastq[:21]
		if nogrid:
			os.system(bwa_cmd)
		else:
			subfile = create_uger_subfile(bwa_cmd, submission_dir, sample_name, "Alignment", queue)
			grid_submit("qsub %s" % (subfile))
	if not nogrid:
		wait_for_previous_step()

def run_tophat(gtf_file, transcriptome_prefix, reference_prefix, read_pairs_file, alignment_dir, 
	       submission_dir, nogrid, se, queue):

	for (fastq_r1, fastq_r2, idx_fastq) in get_read_names(read_pairs_file):
		left_fastq = os.path.join("Reads", fastq_r1)
		if se is 0:
		       	right_fastq = os.path.join("Reads", fastq_r2)
	       	else:
       			right_fastq=""
		basen, extn = fastq_r1.split(".fastq", 1)
		sample_name=basen
		sample_dir = os.path.join(alignment_dir, sample_name)
		soft_mkdir(sample_dir)

		tophat_cmd = make_tophat_cmd(left_fastq, right_fastq, gtf_file, transcriptome_prefix, reference_prefix, sample_dir, se)
		samtools_cmd = "samtools sort %s/accepted_hits.bam %s/%s" % (sample_dir, alignment_dir, basen+".possorted")

		if nogrid:
			os.system(tophat_cmd)
			os.system(samtools_cmd)
		else:
			subfile = create_uger_subfile(tophat_cmd, submission_dir, sample_name, "Alignment", queue)
			subfile = create_uger_subfile(samtools_cmd, submission_dir, sample_name, "Alignment", queue)
			grid_submit("qsub %s" % subfile)
	if not nogrid: 
		wait_for_previous_step()
# Count ----modules 
# -------------------------------------------------------------------------------------------


def run_htseq_count(alignment_dir, counting_dir, submission_dir, gtf_file, nogrid, se):
	global pwd
	method = "union"
	identifier = "gene_name"
	sortedby = "pos"
	if (se == 1):
		stranded = "reverse"
	elif (se == 2):
		stranded = "yes"
	else:
		stranded = "no"
	if nogrid:
		for bam in filter(lambda x: x.endswith('.bam'), os.listdir(alignment_dir)):
			basen, extn = bam.split(".bam", 1)
			os.system("htseq-count -r %s -m %s --stranded=%s -f bam -i %s %s/%s %s > %s/%s.htseq" %
				  (sortedby, method, stranded, identifier, alignment_dir, bam, gtf_file, counting_dir, basen))
	else:
		for bam in filter(lambda x: x.endswith('.bam'), os.listdir(alignment_dir)):
			basen, extn = bam.split(".bam", 1)
			sample_name = basen[0:7]
			count_cmd = "htseq-count -r %s -m %s --stranded=%s -f bam -i %s %s/%s %s > %s/%s.htseq" %
			(sortedby, method, stranded, identifier, alignment_dir, bam, gtf_file, counting_dir, basen)
			subfile = create_uger_subfile(count_cmd, submission_dir, sample_name, "Counting", "short")
			grid_submit("qsub %s" % (subfile))

	merge_cmd = "Rscript %s %s" %(path_from_script_dir("merge_htseq-counts.R"), counting_dir)
	if not nogrid:
		wait_for_previous_step()
		subfile_merge = create_uger_subfile(merge_cmd, submission_dir, "Merge", "MergeCounts", "short")
		grid_submit("qsub %s" % (subfile_merge))
	else:
		os.system(merge_cmd)
		                
def run_count(sym2ref_file, barcode_file, alignment_dir, counting_dir, submission_dir, dge, nogrid):
	count_cmd = make_count_cmd(sym2ref_file, barcode_file, alignment_dir, counting_dir, dge)
	subfile = create_uger_subfile(count_cmd, submission_dir, "3DGE", "Counting", "long")
	if nogrid:
		os.system(count_cmd)
	else:
		qsub_cmd = "qsub %s" % (subfile)
		grid_submit(qsub_cmd)
		wait_for_previous_step()

# QC modules 
# ------------------------------------------------------------------------------------------

def run_rseqc(alignment_dir, rseqc_dir, submission_dir, bed_ref, nogrid):		
	global pwd
	for bam in filter(lambda x: x.endswith('.bam'), os.listdir(alignment_dir)):
	       	basen, extn = bam.split(".bam", 1)
	       	rd_file = os.path.join(rseqc_dir, "ReadsDistribution", basen+".read_dist"+".tsv")
		bs_file = os.path.join(rseqc_dir, "BAMstats", basen+".bam_stat"+".tsv")
		readqc_cmd = "read_distribution.py -i %s/%s -r %s > %s " %(alignment_dir, bam, bed_ref, rd_file)
		bamqc_cmd = "bam_stat.py -i %s/%s > %s" %(alignment_dir, bam, bs_file)
		sed_rd = sed_format(rd_file)
		sed_bs = sed_format(bs_file)
		if nogrid:
			os.system(readqc_cmd)
			os.system(bamqc_cmd)
			os.system(sed_rd)
			os.system(sed_bs)
		else:
			subfile_rd = create_uger_subfile(readqc_cmd, submission_dir, basen, "QC_RD", "short")
#			subfile_bs = create_uger_subfile(bamqc_cmd, submission_dir, basen, "QC_BS", "short")
			subfile_rd = create_uger_subfile(sed_rd, submission_dir, basen, "QC_RD", "short")
#			subfile_bs = create_uger_subfile(sed_bs, submission_dir, basen, "QC_BS", "short")
			grid_submit("qsub %s" % (subfile_rd))
#			grid_submit("qsub %s" % (subfile_bs))

	qc_cmd = "Rscript %s %s"%(path_from_script_dir("qc_BAM.R"), rseqc_dir)
	if not nogrid:
		wait_for_previous_step()
		subfile_qc = create_uger_subfile(qc_cmd, submission_dir, basen, "QC_Merge", "short")
		grid_submit("qsub %s" % (subfile_qc))

	else:
		os.system(qc_cmd)


def run_count_qc(nogrid):
	os.system("Rscript %s ." % path_from_script_dir("transform_counts.R"))
	os.system("Rscript %s ." % path_from_script_dir("qc_analysis.R"))
	if nogrid is not True:
		wait_for_previous_step()

def run_RNASeqC(alignment_dir, qc_dir, reference_fasta, gtf_file):
	os.system("Rscript %s %s" % (path_from_script_dir("merge_htseq-counts.R"), counting_dir))
	os.system("java -jar %s -o %s -s samples_qc.txt -r %s -t %s" % 
		  (path_from_script_dir("RNASeqC.jar"), qc_dir, reference_fasta, gtf_file))

# DE analysis 
# -----------------------------------------------------------------------------------------

def run_edger(edger_dir, rnk_dir, hom_file, org, fa, dge, nogrid):
	global exp_name
	if os.path.isfile("samples_compared.txt"):
		de_analysis_cmd = "Rscript %s . %s" % (path_from_script_dir("de_analysis.R"), dge)
		de_plots_cmd = "Rscript %s . %s %s" % (path_from_script_dir("de_plots.R"), org, fa)
		rnk_cmd = make_rnk_cmd(edger_dir, rnk_dir, hom_file)
		combined_cmd = "%s && %s && %s" % (de_analysis_cmd, de_plots_cmd, rnk_cmd)
		if nogrid:
			os.system(combined_cmd)
		else:
			subfile_de = create_uger_subfile(combined_cmd, "Submission", exp_name, "EdgeR", "short")
			grid_submit("qsub %s"%subfile_de)
			wait_for_previous_step()


def run_nozzle(fa, dge):
	global exp_name
	if dge == 1:
		os.system("Rscript %s . %s" % (path_from_script_dir("nozzle_comp.R"), fa))
	else:
		os.system("Rscript %s . %s %s" % (path_from_script_dir("nozzle.R"), fa, exp_name))


def run_gsea(comparisons_file, gmt_files, rnk_dir, gsea_dir):
	rnk_files = []
	with open(comparisons_file,'r') as comps:
		rnk_files  = [os.path.join(rnk_dir, comp.strip()+".rnk") for comp in comps.readlines()]
	cmd_array = []
	for rnk_file in rnk_files:
		for gmt_file in gmt_files:
			cmd_array.append(make_gsea_cmd(rnk_file, gmt_file, gsea_dir))
	lsf_map.execute(cmd_array, gsea_dir, memreq="1000")

# Main method
# ------------------------------------------------------------------------------------------

def main():

	# argparsing
	parser = argparse.ArgumentParser()
	parser.add_argument("--organism", help="mouse (default) OR human", default="mouse")
	parser.add_argument("--queue", help="queue to use for alignment - long (default) OR short", default="long")
	parser.add_argument("--start", type=int, help="index of starting step", default=0)
	parser.add_argument("--end", type=int, help="index of ending step", default=99)
	parser.add_argument("--nogrid", help="to use UGER or not", action='store_true', default=False)
        parser.add_argument("--fa", help="include this option to show functional analysis results", default=False, action='store_true')
	parser.add_argument("--dge", type=int, help="3 for 3 prime DGE, 5 for 5 prime DGE, 1 for whole transcriptome RNAseq", default=1,required=True)
	parser.add_argument("--se", type=int, help="1 for single end (first strand), 2 for single end(second strand), 0 (default) for paired end", default=0)
	args = parser.parse_args()
	
	org = args.organism
	steps = range(args.start, args.end+1)
	# can also run specific subset, e.g.
	# steps = [1,2,5]
	nogrid = args.nogrid
        fa = args.fa
        dge = int(args.dge)
	se = int(args.se)
	queue = args.queue
	
	print host,pwd,exp_name
	# directories
	reads_dir = "Reads"
	alignment_dir = "Alignment"
	counting_dir = "Counting"
	edger_dir = "EdgeR"
	gsea_dir = "GSEA"
	rnk_dir = "GSEA/Ranks"
	report_dir = "Report"
	qc_dir = "BAMQC"
	rd_dir = "BAMQC/ReadsDistribution"
	bs_dir = "BAMQC/BAMstats"
	submission_dir = "Submission"
	err_dir = "ErrorLogs"
	out_dir = "OutputLogs"

	soft_mkdir(alignment_dir)
	soft_mkdir(counting_dir)
	soft_mkdir(edger_dir)
	soft_mkdir(gsea_dir)
	soft_mkdir(rnk_dir)
	soft_mkdir(report_dir)
	soft_mkdir(qc_dir)
	soft_mkdir(rd_dir)
	soft_mkdir(bs_dir)
	soft_mkdir(submission_dir)
	soft_mkdir(err_dir)
	soft_mkdir(out_dir)

	# input files
	read_pairs = "read_pairs.txt"
	samples_described = "samples_described.txt"
	samples_compared = "samples_compared.txt"
	barcodes = "barcodes.txt"
	os.system("rm Submission/*")

	# organism-specific files
	refseq_file, sym2ref_file, hom_file, reference_prefix, gtf_file, transcriptome_prefix, reference_fasta, bed_file = files_for_organism(org)
	
	# etc files
	gene_sets = ["c5.bp.gmt", "c5.cc.gmt", "c5.mf.gmt"]
	gmt_dir = path_from_script_dir("gmt/")
	gmt_files = [os.path.join(gmt_dir, g) for g in gene_sets]
	counts_file = os.path.join(counting_dir, "counts.umi.dat")
	
	
	# Do stuff
	print host, nogrid
#	assert ((host == "boltzmann" and nogrid) or (host != "boltzmann" and not nogrid)),"If running the pipeline on the grid (UGER), run this script from silver, gold or platinum. Else, run it from boltzmann"

	# TODO something less hacky?
	if 0 in steps:
		notify("Step 0: Validate input")
#		run_validate(read_pairs, barcodes, samples_described, samples_compared, reads_dir)
	if 1 in steps:
		notify("Step 1: Split Fastq")
		if dge > 1:
			run_split(read_pairs, barcodes, reads_dir, alignment_dir, submission_dir, nogrid, dge, queue)
	if 2 in steps:
		notify("Step 2: Align")
                if dge > 1:
		        run_bwa(refseq_file, alignment_dir, submission_dir, nogrid, queue)
                else:
                        run_tophat(gtf_file, transcriptome_prefix, reference_prefix, read_pairs, alignment_dir, submission_dir, nogrid, se, queue)

	if 3 in steps:
		notify("Step 3: Count")
		if dge > 1:
			run_count(sym2ref_file, barcodes, alignment_dir, counting_dir, submission_dir, dge, nogrid)
		else:
			run_htseq_count(alignment_dir, counting_dir, submission_dir, gtf_file, nogrid, se)
	if 4 in steps:
		notify("Step 4: Count QC")
		if dge > 1: 
			run_count_qc(nogrid)
		else:
#			run_RNASeqC(alignment_dir, qc_dir, reference_fasta, gtf_file)
			run_rseqc(alignment_dir, qc_dir, submission_dir, bed_file, nogrid)
	if 5 in steps:
		notify("Step 5: EdgeR")
		run_edger(edger_dir, rnk_dir, hom_file, org, fa, dge, nogrid)
	if 6 in steps:
		notify("Step 6: Generate report")
		run_nozzle(fa, dge)
	# if 7 in steps:
	# 	notify("Step 7: Generate report")
	# 	run_gsea(samples_compared, gmt_files, rnk_dir, gsea_dir)
	
if __name__ == '__main__':
	main()
