import os, shutil, time, subprocess as sub

def setup_logging(cmd_array, working_dir):
	# create empty folder for output files
	log_folder = os.path.join(working_dir,"log")
	if os.path.isdir(log_folder):
		shutil.rmtree(log_folder)
	os.mkdir(log_folder)

	# name output files
	log_array = [os.path.join(log_folder, "job%d.bsub" % ii) for ii in xrange(len(cmd_array))]
	return log_array

def get_io_resource(working_dir):
	# currently we only support a subset of Isilon clusters
	ip_map = {
				"10.200.6.xxx": "argon_io",
				"10.200.7.xxx": "argon_nl_io",
				"10.200.13.xxx": "iodine_io",
				"10.200.1.xxx": "sulfur_io"
			}

	default_resource = "argon_nl_io"
	script = "/broad/tools/scripts/io_resource_for_file"
	ip_address = sub.check_output([script, working_dir]).strip().rstrip("_io")
	ip_block = ".".join(ip_address.split(".")[0:3] + ["xxx"])
	io_resource = ip_map.get(ip_block, default_resource)
	return io_resource

def launch_jobs(cmd_array, log_array, queue="hour", timeout="240", 
	memreq="8000", ioreq="3", io_resource="argon_nl_io"):
	mem = int(memreq)/1000
	bsub_path = "bsub"
	bsub_queue = "-q %s" % queue
	bsub_timeout = "-W %s" % timeout
	bsub_memreq = "-R rusage[mem=%s]" % mem
	bsub_ioreq = "-R rusage[%s=%s]" % (io_resource, ioreq)

	for cmd, log in zip(cmd_array, log_array):
		bsub_log = "-o %s" % log
		bsub_arg = "\"%s\"" % cmd
		bsub_cmd = " ".join([bsub_path, bsub_queue, bsub_timeout, bsub_memreq, 
			bsub_ioreq, bsub_log, bsub_arg])
		os.system(bsub_cmd)

def wait_for_jobs(log_array, period=60):
	# only returns when all jobs are finished 
	# (relies on the fact that bsub does not create log files until jobs terminate)
	all_finished = False
	while not all_finished:
		time.sleep(60)
		status_array = [os.path.isfile(log) for log in log_array]
		all_finished = (status_array.count(False) == 0)

def execute(cmd_array, working_dir, queue="hour", timeout="240", 
	memreq="1000", ioreq="3", checking_period=60):
	
	log_array = setup_logging(cmd_array, working_dir)
	io_resource = get_io_resource(working_dir)
	launch_jobs(cmd_array, log_array, queue=queue, timeout=timeout, 
		memreq=memreq, ioreq=ioreq, io_resource=io_resource)
	wait_for_jobs(log_array, period=checking_period)
