from __future__ import print_function
import os, sys

def bsub_bwa_cmd(fastq_path, reference_prefix, alignment_dir):
    bsub_queue = "hour -W 240"
    bsub_memreq = "\"rusage[mem=4000]\""
    bsub_ioreq = "\"rusage[sulfur_io=3]\""
    bwa_aln_flags = "-l 24"
    bwa_path = "/broad/software/free/Linux/redhat_5_x86_64/pkgs/bwa_0.7.4/bwa"
    bsub_path = "/broad/lsf/7.0/linux2.6-glibc2.3-x86_64/bin/bsub"
    # note: although we specify bsub path explicity, we still need to invoke 'use LSF' before running this code.
    # this is because LSF dotkit usage is more complicated than just adding something to your $PATH.

    bsub = " ".join([bsub_path, "-q", bsub_queue, "-o",  os.path.join(alignment_dir, "bwa.bsub"), "-J bwa", \
                    "-R", bsub_memreq, "-R", bsub_ioreq])
    bwa = " ".join([bwa_path, "aln", bwa_aln_flags, reference_prefix, fastq_path, "|", bwa_path, "samse", \
        reference_prefix, "-", fastq_path, ">", ".".join([fastq_path, "sam"])])

    return bsub+" \""+bwa+"\""

def run_align(species, refseq_dir, alignment_dir):
    reference_prefix_map = {"Human": os.path.join(refseq_dir, "Human_RefSeq", "refMrna_ERCC_polyAstrip.hg19.fa"),
                        "Mouse": os.path.join(refseq_dir, "Mouse_RefSeq", "refMrna_ERCC_polyAstrip.mm10.fa"),
                        "Rat": os.path.join(refseq_dir, "Rat_RefSeq", "refMrna_ERCC_polyAstrip.rn5.fa")}

    reference_prefix = reference_prefix_map[species]
    
    for fastq_name in filter(lambda x: x.endswith('.fastq'), os.listdir(alignment_dir)):
        fastq_path = os.path.join(alignment_dir, fastq_name)
        os.system(bsub_bwa_cmd(fastq_path, reference_prefix, alignment_dir))