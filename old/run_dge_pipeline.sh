#!/bin/bash -l

# This is just a wrapper around the Python script.
# It ensures that all our imports are in order.

reuse -q LSF
reuse -q BWA
reuse -q Python-2.7
reuse -q .GenePattern-1.0.2-r-2.13.1
reuse -q R-3.0

/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts/run_dge_pipeline.py